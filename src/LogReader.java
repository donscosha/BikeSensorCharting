import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LogReader {

	private List<Integer> timeMS;
	private List<Double> accelX;
	private List<Double> accelY;
	private List<Double> accelZ;
	private List<Double> gyroX;
	private List<Double> gyroY;
	private List<Double> gyroZ;
	private List<Double> eulerX;
	private List<Double> eulerY;
	private List<Double> eulerZ;
	
	
	public LogReader() {
		timeMS = new ArrayList<>();
		accelX = new ArrayList<>();
		accelY = new ArrayList<>();
		accelZ = new ArrayList<>();
		gyroX = new ArrayList<>();
		gyroY = new ArrayList<>();
		gyroZ = new ArrayList<>();
		eulerX = new ArrayList<>();
		eulerY = new ArrayList<>();
		eulerZ = new ArrayList<>();
	}

	public void read(String filePath) {
		try {
			RandomAccessFile file = new RandomAccessFile(filePath, "r");
			String str;
			while ((str = file.readLine()) != null) {
				List<String> line = Arrays.asList(str.split(","));
				timeMS.add(Integer.parseInt(line.get(0)));
				accelX.add(Double.parseDouble(line.get(1)));
				accelY.add(Double.parseDouble(line.get(2)));
				accelZ.add(Double.parseDouble(line.get(3)));
				gyroX.add(Double.parseDouble(line.get(4)));
				gyroY.add(Double.parseDouble(line.get(5)));
				gyroZ.add(Double.parseDouble(line.get(6)));
				eulerX.add(Double.parseDouble(line.get(7)));
				eulerY.add(Double.parseDouble(line.get(8)));
				eulerZ.add(Double.parseDouble(line.get(9)));

			}
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public List<Integer> getTimeMS() {
		return timeMS;
	}

	public List<Double> getAccelX() {
		return accelX;
	}

	public List<Double> getAccelY() {
		return accelY;
	}

	public List<Double> getAccelZ() {
		return accelZ;
	}
	
	public List<Double> getGyroX() {
		return gyroX;
	}

	public List<Double> getGyroY() {
		return gyroY;
	}

	public List<Double> getGyroZ() {
		return gyroZ;
	}

	public List<Double> getEulerX() {
		return eulerX;
	}

	public List<Double> getEulerY() {
		return eulerY;
	}

	public List<Double> getEulerZ() {
		return eulerZ;
	}

	public Double getMax(List<Double> list) {
		Double max = list.get(0);
		for (Double d : list) {
			if (d > max) {
				max = d;
			}
		}
		return max;
	}

	public Double getMin(List<Double> list) {
		Double min = list.get(0);
		for (Double d : list) {
			if (d < min) {
				min = d;
			}
		}
		return min;
	}

	public boolean inRange(List<Double> list) {
		return (getMin(list) >= -2.0 && getMax(list) <= 2.0);
	}

}
