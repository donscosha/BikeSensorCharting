import java.util.List;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
 
 
public class SingleSeriesChart extends Application {
 
    @Override public void start(Stage stage) {
    	LogReader lr = new LogReader();
    	lr.read("C:/Users/scott/Documents/Fourthyear/Individual Project/Log Files/TURNS2.TXT");
    	
    	  
        stage.setTitle("Beinn.Bike");
        //defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis(0,360,90);
        
        //creating the chart
        final LineChart<Number,Number> lineChart = 
                new LineChart<Number,Number>(xAxis,yAxis);
                
        lineChart.setTitle("Accelerometer Z");
        //defining a series
        XYChart.Series series = new XYChart.Series();
        series.setName("Accel z");
       

        int i = 1;
        for (Double d : lr.getEulerY()) {
			series.getData().add(new XYChart.Data<>(i,d));
			i++;
		}

        Scene scene  = new Scene(lineChart,2000,800);
        lineChart.getData().add(series);
       
        //Tidy up chart
        lineChart.getXAxis().setTickLabelsVisible(false);
        lineChart.getXAxis().setOpacity(0);
        lineChart.setCreateSymbols(false);
        //Create it
        stage.setScene(scene);
        stage.show();

    }
    
    public static void main(String[] args) {
        launch(args);
        
    }
}