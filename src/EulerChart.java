import java.util.List;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
 
 
public class EulerChart extends Application {
 
    @Override public void start(Stage stage) {
    	LogReader lr = new LogReader();
    	lr.read("C:/Users/scott/Documents/Fourthyear/Individual Project/Log Files/TURNS2.TXT");
    	
    	  
        stage.setTitle("Beinn.Bike");
        //defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis(0,360,60);
        
        //creating the chart
        final LineChart<Number,Number> lineChart = 
                new LineChart<Number,Number>(xAxis,yAxis);
                
        lineChart.setTitle("Pitch, Roll, Yaw");
        
        //defining a series
        XYChart.Series seriesX = new XYChart.Series();
        seriesX.setName("Pitch");
        
        XYChart.Series seriesY = new XYChart.Series();
        seriesY.setName("Roll");
        
        XYChart.Series seriesZ = new XYChart.Series();
        seriesZ.setName("Yaw");
       

        int i = 1;
        for (Double d : lr.getEulerX()) {
			seriesX.getData().add(new XYChart.Data<>(i,d));
			i++;
		}
        
        i = 1;
        for (Double d : lr.getEulerY()) {
			seriesY.getData().add(new XYChart.Data<>(i,d));
			i++;
		}
        
        i = 1;
        for (Double d : lr.getEulerZ()) {
			seriesZ.getData().add(new XYChart.Data<>(i,d));
			i++;
		}

        Scene scene  = new Scene(lineChart,2000,800);
        lineChart.getData().addAll(seriesX,seriesY,seriesZ);
       
        //Tidy up chart
        lineChart.getXAxis().setTickLabelsVisible(false);
        lineChart.getXAxis().setOpacity(0);
        lineChart.setCreateSymbols(false);
        //Create it
        stage.setScene(scene);
        stage.show();

    }
    
    public static void main(String[] args) {
        launch(args);
        
    }
}