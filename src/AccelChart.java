import java.util.List;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
 
 
public class AccelChart extends Application {
 
    @Override public void start(Stage stage) {
    	LogReader lr = new LogReader();
    	lr.read("C:/Users/scott/Documents/Fourthyear/Individual Project/Log Files/TURNS1.TXT");
    	
    	  
        stage.setTitle("Beinn.Bike");
        //defining the axes
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis(-2,2,0.5);
        
        //creating the chart
        final LineChart<Number,Number> lineChart = 
                new LineChart<Number,Number>(xAxis,yAxis);
                
        lineChart.setTitle("Accelerometer XYZ");
        
        //defining a series
        XYChart.Series seriesX = new XYChart.Series();
        seriesX.setName("Accel x");
        
        XYChart.Series seriesY = new XYChart.Series();
        seriesY.setName("Accel y");
        
        XYChart.Series seriesZ = new XYChart.Series();
        seriesZ.setName("Accel z");
       

        int i = 1;
        for (Double d : lr.getAccelX()) {
			seriesX.getData().add(new XYChart.Data<>(i,d));
			i++;
		}
        
        i = 1;
        for (Double d : lr.getAccelY()) {
			seriesY.getData().add(new XYChart.Data<>(i,d));
			i++;
		}
        
        i = 1;
        for (Double d : lr.getAccelZ()) {
			seriesZ.getData().add(new XYChart.Data<>(i,d));
			i++;
		}

        Scene scene  = new Scene(lineChart,2000,800);
        lineChart.getData().addAll(seriesX,seriesY,seriesZ);
       
        //Tidy up chart
        lineChart.getXAxis().setTickLabelsVisible(false);
        lineChart.getXAxis().setOpacity(0);
        lineChart.setCreateSymbols(false);
        //Create it
        stage.setScene(scene);
        stage.show();

    }
    
    
    public static void main(String[] args) {
        launch(args);
        
    }
}